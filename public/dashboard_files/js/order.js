$(document).ready(function () {

    //Add product btn
    $('.add-product-btn').on('click', function (e) {

        e.preventDefault();
        var name = $(this).data('name');
        var id = $(this).data('id');
        var price = $.number($(this).data('price'), 2);

        $(this).removeClass('btn-success').addClass('btn-default disabled');

        // <input type="hidden" name="product_ids[]" value="${id}">
        var html = `<tr>
                        <td>${name}</td>
                        <td><input type="number" data-price="${price}" name="products[${id}][quantity]" class="form-control input-sm product-quantity" min="1" value="1"></td>
                        <td class="product-price">${price}</td>
                        <td><button class="btn btn-danger btn-sm remove-product-btn" data-id="${id}"><span class="fa fa-trash"></span></button></td>
                    </tr>`;

        $('.order-list').append(html);

        //to calculate total
        calculateTotal();

    }); //end of add product btn

    //Disabled btn
    $('body').on('click', '.disabled', function (e) {
        e.preventDefault();
    });
    //end of Disabled btn

    // $('.remove-product-btn').on('click', function () { alert('34343'); });

    //Remove product btn
    $('body').on('click', '.remove-product-btn', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $(this).closest('tr').remove();
        $('#product-' + id).removeClass('btn-default disabled').addClass('btn-success');

        //to calculate total
        calculateTotal();

    }); //end of remove product btn

    //Change product quantity
    $('body').on('keyup change', '.product-quantity', function () {

        var quantity = $(this).val();

        var unitPrice = parseFloat($(this).data('price').replace(/,/g, ''))
        //var unitPrice = $(this).data('price');

        var total = quantity * unitPrice;

        $(this).closest('tr').find('.product-price').html($.number(total, 2));

        calculateTotal();
    }); //end of product quantity

    //order products click
    $('.order-products').on('click', function (e) {
        e.preventDefault();
        $('#loading').css('display', 'flex');
        var url = $(this).data('url');
        var method = $(this).data('method');
        $.ajax({
            url : url,
            method : method,
            success : function (data) {
                $('#loading').css('display', 'none');
                $('#order-product-list').empty();
                $('#order-product-list').append(data);
            }
        });
    }); //end of order products click

    $('body').on('click', '.print-btn', function (e) {
        e.preventDefault();
        $('#print-area').printThis({
            'header' : "<h1>Orders</h1>",
            'pageTitle': "Order List",
            "footer": "POS.com"
        });
    });
}); //end of document ready


//calculate the total
function calculateTotal() {

    var price = 0;
    $('.order-list .product-price').each(function (index) {
        price += parseFloat($(this).html().replace(/,/g, ''));
    });
    $('.total-price').html($.number(price, 2));

    if(price > 0){
        $('#add-order-form-btn').removeClass('disabled');
    }else{
        $('#add-order-form-btn').addClass('disabled');
    }
}
