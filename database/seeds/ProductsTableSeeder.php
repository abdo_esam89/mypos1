<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products_name_desc_ar = [
            'name' => ['المنتج الثالث', 'المنتج الرابع', 'طابعة كمبيوتر'],
            'desc' => ['وصف المنتج الثالث وصف المنتج الثالث', 'وصف المنتج الرابع', 'وصف طابعة الكمبيوتر']
        ];
        $products_name_desc_en = [
            'name' => ['Third Product', 'Fourth Product', 'Printer'],
            'desc' => ['description for third product', 'description for fourth product', 'description for printer']
        ];
        $products_purchase_price = [ 100, 200, 300 ];
        $products_sale_price = [ 200, 300, 400.50 ];

        foreach ($products_name_desc_ar['name'] as $key => $value){
            Product::create([
                'category_id' => 1,
                'ar' => ['name'=> $products_name_desc_ar['name'][$key], 'description'=> $products_name_desc_ar['desc'][$key]],
                'en' => ['name'=> $products_name_desc_en['name'][$key], 'description'=> $products_name_desc_en['desc'][$key]],
                'purchase_price' => $products_purchase_price[$key],
                'sale_price' => $products_sale_price[$key],
                'stock' => 50
            ]);
            $key++;
        }
    }
}
