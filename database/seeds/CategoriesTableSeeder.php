<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories_ar = ['القسم الأول' , 'القسم الثاني', 'القسم الثالث'];
        $categories_en = ['First Category' , 'Second Category', 'Third Category'];

        for ($i = 0; $i < count($categories_ar); $i++){
            Category::create([
                'ar' => ['name' => $categories_ar[$i]],
                'en' => ['name' => $categories_en[$i]],
            ]);
        }
    }
}
