@extends('layouts.dashboard.app')

@section('content')


    <div class="content-wrapper">


        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('site.clients')</h1>
                    </div>

                    <div class="col-sm-6">
                        <ol class="breadcrumb @if(app()->getLocale() !== 'ar') float-sm-right @else float-sm-left @endif">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.welcome') }}">@lang('site.dashboard')</a></li>
                            <li class="breadcrumb-item active">@lang('site.clients')</li>
                        </ol>
                    </div>


                </div>
            </div>
        </section>



        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <h3 class="card-title">@lang('site.clients_table')<small style="margin: 0 13px;font-weight: bold;">{{ ' ( ' . $clients->count() . ' ) ' }}</small></h3>
                                <br>

                                <form action="{{ route('dashboard.clients.index') }}" method="GET">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" value="{{ request()->search }}">
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> @lang('site.search')</button>
                                            @if(auth()->user()->hasPermission('create_clients'))
                                                <a href="{{ route('dashboard.clients.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</a>
                                            @else
                                                <a href="#" class="btn btn-info" disabled><i class="fa fa-plus"></i> @lang('site.add')</a>
                                            @endif
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                @if($clients->count() > 0)
                                    <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>@lang('site.name')</th>
                                        <th>@lang('site.phone')</th>
                                        <th>@lang('site.address')</th>
                                        <th>@lang('site.add_order')</th>
                                        <th>@lang('site.options')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($clients as $index => $client)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $client->name }}</td>
                                        <td>{{ is_array($client->phone) ? implode($client->phone, ' - ') : $client->phone }}</td>
                                        <td>{{ $client->address }}</td>
                                        <td>
                                            @if(auth()->user()->hasPermission('create_orders'))
                                                <a href="{{ route('dashboard.clients.orders.create', $client->id) }}" class="btn btn-primary btn-sm">@lang('site.add_order')</a>
                                            @else
                                                <a href="#" class="btn btn-info btn-sm" disabled>@lang('site.add_order')</a>
                                            @endif
                                        </td>
                                        <td>
                                            @if(auth()->user()->hasPermission('update_clients'))
                                                <a href="{{ route('dashboard.clients.edit', $client->id) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> @lang('site.edit')</a>
                                            @else
                                                <a href="#" class="btn btn-info btn-sm" disabled><i class="fa fa-edit"></i> @lang('site.edit')</a>
                                            @endif
                                            @if(auth()->user()->hasPermission('delete_clients'))
                                                <form action="{{ route('dashboard.clients.destroy', $client->id) }}" method="POST" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            @else
                                                <button class="btn btn-danger btn-sm disabled"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                                            @endif


                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <br>

                                    {{ $clients->appends(request()->query())->links() }}
                                @else
                                    <h2>@lang('site.no_data_found')</h2>
                                @endif
                            </div>
                            <!-- /.card-body -->
                            {{--<div class="card-footer clearfix">--}}
                                {{--<ul class="pagination pagination-sm m-0 float-right">--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">&laquo;</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">1</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">&raquo;</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        </div>
                        <!-- /.card -->


                    </div>
                    <!-- /.col -->

                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>




    </div>

@endsection
