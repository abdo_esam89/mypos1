@extends('layouts.dashboard.app')

@section('content')

<div class="content-wrapper">


<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>@lang('site.clients')</h1>
            </div>

            <div class="col-sm-6">
                <ol class="breadcrumb @if(app()->getLocale() !== 'ar') float-sm-right @else float-sm-left @endif">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard.welcome') }}">@lang('site.dashboard')</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard.clients.index') }}">@lang('site.clients')</a></li>
{{--                    <li class="breadcrumb-item"><a href="{{ route('dashboard.clients.orders') }}">@lang('site.orders')</a></li>--}}
                    <li class="breadcrumb-item active">@lang('site.add')</li>
                </ol>
            </div>


        </div>
    </div>
</section>



<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card">

                    <div class="card-header with-border">
                        <h2 class="card-title">@lang('site.categories')</h2>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body with-border">

                        @include('partials._errors')

                        @foreach ($categories as $category)

                            <div class="panel-group" style="margin-bottom: 14px;">

                                <div class="panel panel-info" style="background-color: lavender;border: 1px solid #60a2f3;border-radius: 4px;padding: 0 4px;">

                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#{{ str_replace(' ', '-', $category->name) }}" style="font-size: 70%">{{ $category->name }}</a>
                                        </h4>
                                    </div>

                                    <div id="{{ str_replace(' ', '-', $category->name) }}" class="panel-collapse collapse">

                                        <div class="panel-body">

                                            @if ($category->products->count() > 0)

                                                <table class="table table-hover">
                                                    <tr>
                                                        <th>@lang('site.name')</th>
                                                        <th>@lang('site.stock')</th>
                                                        <th>@lang('site.price')</th>
                                                        <th>@lang('site.add')</th>
                                                    </tr>

                                                    @foreach ($category->products as $product)
                                                        <tr>
                                                            <td>{{ $product->name }}</td>
                                                            <td>{{ $product->stock }}</td>
                                                            <td>{{ number_format($product->sale_price, 2) }}</td>
                                                            <td>
                                                                <a href=""
                                                                   id="product-{{ $product->id }}"
                                                                   data-name="{{ $product->name }}"
                                                                   data-id="{{ $product->id }}"
                                                                   data-price="{{ $product->sale_price }}"
                                                                   class="btn btn-success btn-sm add-product-btn">
                                                                    <i class="fa fa-plus"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                </table><!-- end of table -->

                                            @else
                                                <h5>@lang('site.no_records')</h5>
                                            @endif

                                        </div><!-- end of panel body -->

                                    </div><!-- end of panel collapse -->

                                </div><!-- end of panel primary -->

                            </div><!-- end of panel group -->

                        @endforeach

                    </div>
                    <!-- /.card-body -->

                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header with-border">
                        <h2 class="card-title">@lang('site.orders')</h2>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body with-border">
                        @include('partials._errors')
                        <form action="{{ route('dashboard.clients.orders.store', $client->id) }}" method="post">

                            {{ csrf_field() }}
                            {{ method_field('post') }}

                            @include('partials._errors')

                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>@lang('site.product')</th>
                                    <th>@lang('site.quantity')</th>
                                    <th>@lang('site.price')</th>
                                    <th>@lang('site.options')</th>
                                </tr>
                                </thead>

                                <tbody class="order-list">


                                </tbody>

                            </table><!-- end of table -->

                            <h4>@lang('site.total') : <span class="total-price">0</span></h4>

                            <button class="btn btn-primary btn-block disabled" id="add-order-form-btn"><i class="fa fa-plus"></i> @lang('site.add_order')</button>

                        </form>

                    </div>
                    <!-- /.card-body -->
                    @if ($client->orders->count() > 0)
                    <div class="card-header with-border">
                        <h2 class="card-title">@lang('site.previous_orders')<small> {{ "( " .$orders->total(). " )" }} </small></h2>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body with-border">

                        @foreach ($orders as $order)

                            <div class="panel-group" style="margin-bottom: 14px;">

                                <div class="panel panel-info" style="background-color: lavender;border: 1px solid #60a2f3;border-radius: 4px;padding: 0 4px;">

                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#created_{{ $order->created_at->format('d-m-Y-s') }}" style="font-size: 70%">{{ $order->created_at->toFormattedDateString() }} - {{ $order->created_at->diffForHumans() }}</a>
                                        </h4>
                                    </div>
                                    <div id="created_{{ $order->created_at->format('d-m-Y-s') }}" class="panel-collapse collapse">

                                        <div class="panel-body">
                                                <table class="table table-hover">
                                                    <tr style="border-bottom: 2px solid;border-top: 2px solid;">
                                                        <th>@lang('site.name')</th>
                                                    </tr>
                                                    @foreach ($order->products as $product)
                                                        <tr>
                                                            <td>{{ $product->name }}</td>
                                                        </tr>
                                                    @endforeach

                                                </table><!-- end of table -->

                                        </div><!-- end of panel body -->


                                    </div><!-- end of panel collapse -->

                                </div><!-- end of panel primary -->

                            </div><!-- end of panel group -->

                        @endforeach
                        {{ $orders->links() }}







                    </div>
                    <!-- /.card-body -->
                    @endif
                </div>
                <!-- /.card -->


            </div>
        </div>
        <!-- /.row -->

    </div><!-- /.container-fluid -->
</section>




</div>

@endsection
