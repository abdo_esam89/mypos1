@extends('layouts.dashboard.app')

@section('content')

<div class="content-wrapper">


<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>@lang('site.clients')</h1>
            </div>

            <div class="col-sm-6">
                <ol class="breadcrumb @if(app()->getLocale() !== 'ar') float-sm-right @else float-sm-left @endif">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard.welcome') }}">@lang('site.dashboard')</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard.clients.index') }}">@lang('site.clients')</a></li>
                    <li class="breadcrumb-item active">@lang('site.add')</li>
                </ol>
            </div>


        </div>
    </div>
</section>



<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header with-border">
                        <h2 class="card-title">@lang('site.add_client')</h2>


                    </div>
                    <!-- /.card-header -->
                    <div class="card-body with-border">

                        @include('partials._errors')

                        <form action="{{ route('dashboard.clients.store') }}" method="POST">

                            {{ csrf_field() }}
                            {{ method_field('post') }}

                            <div class="form-group">
                                <label>@lang('site.name')</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                            </div>

                            @for($i = 0; $i < 2; $i++)

                            <div class="form-group">
                                <label>@lang('site.phone')</label>
                                <input type="text" name="phone[]" class="form-control">
                            </div>

                            @endfor

                            <div class="form-group">
                                <label>@lang('site.address')</label>
                                <textarea name="address" class="form-control">{{ old('address') }}</textarea>
                            </div>


                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                            </div>

                        </form>

                    </div>
                    <!-- /.card-body -->

                </div>
                <!-- /.card -->


            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

    </div><!-- /.container-fluid -->
</section>




</div>

@endsection
