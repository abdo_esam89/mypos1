@extends('layouts.dashboard.app')

@section('content')

    <div class="content-wrapper">


        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('site.users')</h1>
                    </div>

                    <div class="col-sm-6">
                        <ol class="breadcrumb @if(app()->getLocale() !== 'ar') float-sm-right @else float-sm-left @endif">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.welcome') }}">@lang('site.dashboard')</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.users.index') }}">@lang('site.users')</a></li>
                            <li class="breadcrumb-item active">@lang('site.edit')</li>
                        </ol>
                    </div>


                </div>
            </div>
        </section>



        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <h2 class="card-title">@lang('site.edit_user')</h2>


                            </div>
                            <!-- /.card-header -->
                            <div class="card-body with-border">

                                @include('partials._errors')

                                <form action="{{ route('dashboard.users.update', $user->id) }}" method="POST" enctype="multipart/form-data">

                                    {{ csrf_field() }}
                                    {{ method_field('put') }}


                                    <div class="form-group">
                                        <label>@lang('site.first_name')</label>
                                        <input type="text" name="first_name" class="form-control" value="{{ $user->first_name }}">
                                    </div>

                                    <div class="form-group">
                                        <label>@lang('site.last_name')</label>
                                        <input type="text" name="last_name" class="form-control" value="{{ $user->last_name }}">
                                    </div>

                                    <div class="form-group">
                                        <label>@lang('site.email')</label>
                                        <input type="email" name="email" class="form-control" value="{{ $user->email }}">
                                    </div>

                                    <div class="form-group">
                                        <label>@lang('site.image')</label>
                                        <input type="file" name="image" class="form-control image">
                                    </div>

                                    <div class="form-group">
                                        <img src="{{ $user->image_path }}" style="width: 120px;" class="img-thumbnail image-preview" alt="">
                                    </div>

                                    {{--<div class="form-group">--}}
                                        {{--<label>@lang('site.password')</label>--}}
                                        {{--<input type="password" name="password" class="form-control">--}}
                                    {{--</div>--}}

                                    {{--<div class="form-group">--}}
                                        {{--<label>@lang('site.password_confirmation')</label>--}}
                                        {{--<input type="password" name="password_confirmation" class="form-control" value="{{ old('password_confirmation') }}">--}}
                                    {{--</div>--}}

                                    <div class="form-group">
                                        <label>@lang('site.permissions')</label>
                                        <div class="card card-primary card-outline card-outline-tabs">

                                            @php

                                                $models = ['users', 'categories', 'products', 'clients', 'orders'];

                                                $maps   = ['create', 'read', 'update', 'delete'];

                                            @endphp

                                            <div class="card-header p-0 border-bottom-0">
                                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">

                                                    @foreach($models as $index => $model)
                                                        <li class="nav-item">
                                                            <a class="nav-link {{ ( $index == 0 ) ? 'active' : ''}}" id="custom-tabs-three-home-tab"
                                                               data-toggle="pill" href="#{{ $model }}" role="tab" aria-controls="custom-tabs-three-home"
                                                               aria-selected="true">@lang('site.'.$model)</a>
                                                        </li>

                                                    @endforeach
                                                    {{--<li class="nav-item">--}}
                                                    {{--<a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#tab_2" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Profile</a>--}}
                                                    {{--</li>--}}

                                                </ul>
                                            </div>

                                            <div class="card-body">
                                                <div class="tab-content" id="custom-tabs-three-tabContent">

                                                    @foreach($models as $index => $model)
                                                        <div class="tab-pane fade {{ ( $index == 0 ) ? 'active show' : ''}}" id="{{ $model }}" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">

                                                            @foreach($maps as $map)
                                                                <label class="container1">
                                                                    <input name="permissions[]" value="{{$map.'_'.$model }}" type="checkbox" {{ $user->hasPermission($map.'_'.$model) ? 'checked' : '' }}>@lang('site.'.$map)
                                                                    <span class="checkmark1"></span>
                                                                </label>
                                                            @endforeach



                                                        </div>
                                                        {{--<div class="tab-pane fade" id="tab_2" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">--}}
                                                        {{--Mauris tincidunt mi at erat gravida, tab2--}}
                                                        {{--</div>--}}
                                                    @endforeach

                                                </div>
                                            </div>
                                            <!-- /.card -->
                                        </div>

                                    </div>





                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> @lang('site.edit')</button>
                                    </div>

                                </form>

                            </div>
                            <!-- /.card-body -->

                        </div>
                        <!-- /.card -->


                    </div>
                    <!-- /.col -->

                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>




    </div>

@endsection
