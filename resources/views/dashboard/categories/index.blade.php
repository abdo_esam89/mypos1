@extends('layouts.dashboard.app')

@section('content')


    <div class="content-wrapper">


        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('site.categories')</h1>
                    </div>

                    <div class="col-sm-6">
                        <ol class="breadcrumb @if(app()->getLocale() !== 'ar') float-sm-right @else float-sm-left @endif">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.welcome') }}">@lang('site.dashboard')</a></li>
                            <li class="breadcrumb-item active">@lang('site.categories')</li>
                        </ol>
                    </div>


                </div>
            </div>
        </section>



        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <h3 class="card-title">@lang('site.categories_table')<small style="margin: 0 13px;font-weight: bold;">{{ ' ( ' . $categories->count() . ' ) ' }}</small></h3>
                                <br>

                                <form action="{{ route('dashboard.categories.index') }}" method="GET">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" value="{{ request()->search }}">
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> @lang('site.search')</button>
                                            @if(auth()->user()->hasPermission('create_categories'))
                                                <a href="{{ route('dashboard.categories.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</a>
                                            @else
                                                <a href="#" class="btn btn-info" disabled><i class="fa fa-plus"></i> @lang('site.add')</a>
                                            @endif
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                @if($categories->count() > 0)
                                    <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>@lang('site.name')</th>
                                        <th>@lang('site.products_count')</th>
                                        <th>@lang('site.products_related')</th>
                                        <th>@lang('site.options')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $index => $category)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->products->count() }}</td>
                                        <td><a href="{{ route('dashboard.products.index', ['category_id' => $category->id]) }}" class="btn btn-default">@lang('site.related_products')</a></td>
                                        <td>
                                            @if(auth()->user()->hasPermission('update_categories'))
                                                <a href="{{ route('dashboard.categories.edit', $category->id) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> @lang('site.edit')</a>
                                            @else
                                                <a href="#" class="btn btn-info btn-sm" disabled><i class="fa fa-edit"></i> @lang('site.edit')</a>
                                            @endif
                                            @if(auth()->user()->hasPermission('delete_categories'))
                                                <form action="{{ route('dashboard.categories.destroy', $category->id) }}" method="POST" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            @else
                                                <button class="btn btn-danger btn-sm disabled"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                                            @endif


                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <br>

                                    {{ $categories->appends(request()->query())->links() }}
                                @else
                                    <h2>@lang('site.no_data_found')</h2>
                                @endif
                            </div>
                            <!-- /.card-body -->
                            {{--<div class="card-footer clearfix">--}}
                                {{--<ul class="pagination pagination-sm m-0 float-right">--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">&laquo;</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">1</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">&raquo;</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        </div>
                        <!-- /.card -->


                    </div>
                    <!-- /.col -->

                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>




    </div>

@endsection
