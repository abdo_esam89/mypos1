@extends('layouts.dashboard.app')

@section('content')

<div class="content-wrapper">


<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>@lang('site.users')</h1>
            </div>

            <div class="col-sm-6">
                <ol class="breadcrumb @if(app()->getLocale() !== 'ar') float-sm-right @else float-sm-left @endif">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard.welcome') }}">@lang('site.dashboard')</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard.categories.index') }}">@lang('site.categories')</a></li>
                    <li class="breadcrumb-item active">@lang('site.add')</li>
                </ol>
            </div>


        </div>
    </div>
</section>



<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header with-border">
                        <h2 class="card-title">@lang('site.add_category')</h2>


                    </div>
                    <!-- /.card-header -->
                    <div class="card-body with-border">

                        @include('partials._errors')

                        <form action="{{ route('dashboard.categories.store') }}" method="POST">

                            {{ csrf_field() }}
                            {{ method_field('post') }}

                            @foreach(config('translatable.locales') as $locale)

                            <div class="form-group">
                                <label>@lang('site.'.$locale.'.name')</label>
                                <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ old($locale.'.name') }}">
                            </div>

                            @endforeach





                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                            </div>

                        </form>

                    </div>
                    <!-- /.card-body -->

                </div>
                <!-- /.card -->


            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

    </div><!-- /.container-fluid -->
</section>




</div>

@endsection
