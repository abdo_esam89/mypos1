@extends('layouts.dashboard.app')

@section('content')


    <div class="content-wrapper">


        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('site.products')</h1>
                    </div>

                    <div class="col-sm-6">
                        <ol class="breadcrumb @if(app()->getLocale() !== 'ar') float-sm-right @else float-sm-left @endif">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.welcome') }}">@lang('site.dashboard')</a></li>
                            <li class="breadcrumb-item active">@lang('site.products')</li>
                        </ol>
                    </div>


                </div>
            </div>
        </section>



        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header with-border">
                                <h3 class="card-title">@lang('site.products_table')<small style="margin: 0 13px;font-weight: bold;">{{ ' ( ' . $products->count() . ' ) ' }}</small></h3>
                                <br>

                                <form action="{{ route('dashboard.products.index') }}" method="GET">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" value="{{ request()->search }}">
                                        </div>

                                        <div class="col-md-4">
                                            <select name="category_id" class="form-control">
                                                <option value="">@lang('site.all_categories')</option>
                                                @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}" {{ request()->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> @lang('site.search')</button>
                                            @if(auth()->user()->hasPermission('create_products'))
                                                <a href="{{ route('dashboard.products.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</a>
                                            @else
                                                <a href="#" class="btn btn-info" disabled><i class="fa fa-plus"></i> @lang('site.add')</a>
                                            @endif
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                @if($products->count() > 0)
                                    <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>@lang('site.name')</th>
                                        <th width="15%">@lang('site.description')</th>
                                        <th>@lang('site.image')</th>
                                        <th>@lang('site.category')</th>
                                        <th>@lang('site.purchase_price')</th>
                                        <th>@lang('site.sale_price')</th>
                                        <th>@lang('site.profit_percent') %</th>
                                        <th>@lang('site.stock')</th>
                                        <th>@lang('site.options')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $index => $product)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td width="15%">{!! Str::limit($product->description, 50) !!}</td>
                                        <td width="6%"><img src="{{ $product->image_path }}" style="width: 100px; max-height: 60px;" class="img-thumbnail" alt=""></td>
                                        <td>{{ $product->category->name }}</td>
                                        <td>{{ $product->purchase_price }}</td>
                                        <td>{{ $product->sale_price }}</td>
                                        <td>{{ $product->profit_percent }} %</td>
                                        <td>{{ $product->stock }}</td>
                                        <td>
                                            @if(auth()->user()->hasPermission('update_products'))
                                                <a href="{{ route('dashboard.products.edit', $product->id) }}" class="btn btn-info btn-sm" style="font-size: 0.75em;"><i class="fa fa-edit"></i> @lang('site.edit')</a>
                                            @else
                                                <a href="#" class="btn btn-info btn-sm" disabled style="font-size: 0.75em;"><i class="fa fa-edit"></i> @lang('site.edit')</a>
                                            @endif
                                            @if(auth()->user()->hasPermission('delete_products'))
                                                <form action="{{ route('dashboard.products.destroy', $product->id) }}" method="POST" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button type="submit" class="btn btn-danger btn-sm delete" style="font-size: 0.75em;"><i class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            @else
                                                <button class="btn btn-danger btn-sm disabled" style="font-size: 0.75em;"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                                            @endif


                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <br>

                                    {{ $products->appends(request()->query())->links() }}
                                @else
                                    <h2>@lang('site.no_data_found')</h2>
                                @endif
                            </div>
                            <!-- /.card-body -->
                            {{--<div class="card-footer clearfix">--}}
                                {{--<ul class="pagination pagination-sm m-0 float-right">--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">&laquo;</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">1</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                                    {{--<li class="page-item"><a class="page-link" href="#">&raquo;</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        </div>
                        <!-- /.card -->


                    </div>
                    <!-- /.col -->

                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>




    </div>

@endsection
