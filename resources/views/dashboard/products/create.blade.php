@extends('layouts.dashboard.app')

@section('content')

<div class="content-wrapper">


<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>@lang('site.users')</h1>
            </div>

            <div class="col-sm-6">
                <ol class="breadcrumb @if(app()->getLocale() !== 'ar') float-sm-right @else float-sm-left @endif">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard.welcome') }}">@lang('site.dashboard')</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('dashboard.products.index') }}">@lang('site.products')</a></li>
                    <li class="breadcrumb-item active">@lang('site.add')</li>
                </ol>
            </div>


        </div>
    </div>
</section>



<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header with-border">
                        <h2 class="card-title">@lang('site.add_category')</h2>


                    </div>
                    <!-- /.card-header -->
                    <div class="card-body with-border">

                        @include('partials._errors')

                        <form action="{{ route('dashboard.products.store') }}" method="POST" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            {{ method_field('post') }}

                            <div class="form-group">
                                <label>@lang('site.categories')</label>
                                <select name="category_id" class="form-control">
                                    <option value="">@lang('site.all_categories')</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            @foreach(config('translatable.locales') as $locale)

                                <div class="form-group">
                                    <label>@lang('site.'.$locale.'.name')</label>
                                    <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ old($locale.'.name') }}">
                                </div>

                                <div class="form-group">
                                    <label>@lang('site.'.$locale.'.description')</label>
                                    <textarea name="{{ $locale }}[description]" class="form-control ckeditor">{{ old($locale.'.description') }}</textarea>
                                </div>

                            @endforeach


                            <div class="form-group">
                                <label>@lang('site.image')</label>
                                <input type="file" name="image" class="form-control image">
                            </div>

                            <div class="form-group">
                                <img src="{{ asset('uploads/product_images/default.png') }}" style="width: 100px" class="img-thumbnail image-preview" alt="">
                            </div>

                            <div class="form-group">
                                <label>@lang('site.purchase_price')</label>
                                <input type="number" name="purchase_price" step="0.01" class="form-control" value="{{ old('purchase_price') }}">
                            </div>

                            <div class="form-group">
                                <label>@lang('site.sale_price')</label>
                                <input type="number" name="sale_price" step="0.01" class="form-control" value="{{ old('sale_price') }}" >
                            </div>

                            <div class="form-group">
                                <label>@lang('site.stock')</label>
                                <input type="number" name="stock" class="form-control" value="{{ old('stock') }}">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                            </div>

                        </form>

                    </div>
                    <!-- /.card-body -->

                </div>
                <!-- /.card -->


            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->

    </div><!-- /.container-fluid -->
</section>




</div>

@endsection
