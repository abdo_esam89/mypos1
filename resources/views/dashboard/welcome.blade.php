@extends('layouts.dashboard.app')

@section('content')


    <div class="content-wrapper">


        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>@lang('site.dashboard')</h1>
                    </div>

                    <div class="col-sm-6">
                        <ol class="breadcrumb @if(app()->getLocale() !== 'ar') float-sm-right @else float-sm-left @endif"">
                            <li class="breadcrumb-item active"><a href="{{ route('dashboard.welcome') }}">@lang('site.dashboard')</a></li>
                        </ol>
                    </div>


                </div>
            </div>
        </section>



        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{ $categories_count }}</h3>

                                <p>@lang('site.categories')</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="{{ route('dashboard.categories.index') }}" class="small-box-footer">@lang('site.read') <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>{{ $products_count }}</h3>

                                <p>@lang('site.products')</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="{{ route('dashboard.products.index') }}" class="small-box-footer">@lang('site.read') <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{ $clients_count }}</h3>

                                <p>@lang('site.clients')</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="{{ route('dashboard.clients.index') }}" class="small-box-footer">@lang('site.read') <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>{{ $users_count }}</h3>

                                <p>@lang('site.users')</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="{{ route('dashboard.users.index') }}" class="small-box-footer">@lang('site.read') <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->

                </div>
                <style>
                    .bg-gradient-info {
                        background: #17a2b8 linear-gradient(180deg, #3ab0c3, #17a2b8) repeat-x !important;
                    }
                </style>
                <div class="row">
                    <div class="col-md-12">
                    <!-- solid sales graph -->
                    <div class="card bg-gradient-info">
                        <div class="card-header border-0">
                            <h3 class="card-title" style="color: #fff;">
                                <i class="fas fa-th mr-1"></i>
                                @lang('site.sales_graph')
                            </h3>

                            <div class="card-tools">
                                <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn bg-info btn-sm" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <canvas class="chart" id="line-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                        <!-- /.card-body -->

                        <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                    </div>
                </div>

            </div>
        </section>

    </div>

@endsection
@push('scripts')
    <script>

        // Sales graph chart
        var salesGraphChartCanvas = $('#line-chart').get(0).getContext('2d');
        //$('#revenue-chart').get(0).getContext('2d');

        var salesGraphChartData = {
            labels  : [
                @foreach ($sales_data as $data)
                    '{{$data->year}} {{$data->month}}',
                @endforeach
            ],
            datasets: [
                {
                    label               : ['@lang('site.total')'],
                    fill                : false,
                    borderWidth         : 2,
                    lineTension         : 0,
                    spanGaps : true,
                    borderColor         : '#efefef',
                    pointRadius         : 3,
                    pointHoverRadius    : 7,
                    pointColor          : '#efefef',
                    pointBackgroundColor: '#efefef',
                    data                : [
                        @foreach ($sales_data as $data)
                            {{ $data->sum }},
                        @endforeach
                    ]
                }
            ]
        }

        var salesGraphChartOptions = {
            maintainAspectRatio : false,
            responsive : true,
            legend: {
                display: false,
            },
            scales: {
                xAxes: [{
                    ticks : {
                        fontColor: '#efefef',
                    },
                    gridLines : {
                        display : false,
                        color: '#efefef',
                        drawBorder: false,
                    }
                }],
                yAxes: [{
                    ticks : {
                        stepSize: 800,
                        fontColor: '#efefef',
                    },
                    gridLines : {
                        display : true,
                        color: '#efefef',
                        drawBorder: true,
                    }
                }]
            }
        }

        // This will get the first returned node in the jQuery collection.
        var salesGraphChart = new Chart(salesGraphChartCanvas, {
                type: 'line',
                data: salesGraphChartData,
                options: salesGraphChartOptions
            }
        )
    </script>
@endpush
