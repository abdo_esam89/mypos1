<?php
return [

    "dashboard"             => 'الرئيسية',
    "dashboard_admin_panel" => 'لوحة تحكم الإدارة',
    "dashboard_home"        => 'الرئيسية',
    "dashboard_contact"     => 'اتصل بنا',
    "dashboard_search"      => 'ابحث',
    "admin_panel"           => 'لوحة تحكم الإدارة',
    "sales_graph"           => 'الرسم البياني',
    "remember_me"           => 'تذكرني',
    "sign_in_to_start"      => 'سجل بيانات الدخول',
    "add"                   => 'أضف',
    "edit"                  => 'تعديل',
    "delete"                => 'حذف',
    "create"                => 'إضافة',
    "read"                  => 'عرض',
    "update"                => 'تعديل',
    "login"                 => 'تسجيل الدخول',

    "no_data_found"         => 'ﻻ يوجد بيانات مسجلة',
    "no_records"            => 'ﻻ يوجد بيانات مسجلة',

    'clients'               => 'العملاء',
    'client_name'           => 'اسم العميل',
    'add_client'            => 'إضافة عميل',
    'clients_table'         => 'جدول العملاء',
    'phone'                 => 'التلفون',
    'address'               => 'العنوان',
    'previous_orders'       => 'الطلبات السابقة',
    'orders'                => 'الطلبات',
    'add_order'             => 'أضف طلب',
    'edit_order'            => 'تعديل طلب',
    'order'                 => 'طلب',
    'show'                  => 'عرض',
    'loading'               => 'جاري التحميل',
    'print'                 => 'طباعة',

    "users"                 => 'المشرفون',
    "first_name"            => 'الاسم اﻷول',
    "last_name"             => 'الاسم اﻷخير',
    "email"                 => 'البريد اﻻلكتروني',
    "options"               => 'التحكم',
    "users_table"           => 'جدول المشرفين',
    "search"                => 'بحث',
    "password"              => 'كلمة المرور',
    "password_confirmation" => 'تأكيد كلمة المرور',
    "add_user"              => 'إضافة مستخدم جديد',
    "edit_user"             => 'تعديل بيانات عضو',
    "image"                 => 'صورة',

    "added_successfully"    => 'تم إضافة البيانات بنجاح',
    "updated_successfully"   => 'تم تعديل البيانات بنجاح',
    "deleted_successfully"  => 'تم حذف البيانات بنجاح',

    'permissions'           => 'الصلاحيات',

    'categories'            => 'اﻷقسام',
    'categories_table'      => 'جدول اﻷقسام',
    'all_categories'        => 'كل الأقسام',
    'name'                  => 'الاسم',
    'add_category'          => 'إضافة قسم',
    'description'           => 'الوصف',
    'products_count'        => 'عدد المنتجات',
    'related_products'      => 'المنتجات المرتبطة',
    'category'              => 'القسم',
    'show_products'         => 'عرض المنتجات',
    'created_at'            => 'تم إضافته',

    'no'                    => 'ﻻ',
    'yes'                   => 'نعم',
    'confirm_delete'        => 'هل تريد بالتأكيد الحذف ؟',

    'products'              => 'المنتجات',
    'edit_product'          => 'تعديل منتج',
    'products_table'        => 'جدول المنتجات',
    'products_related'      => 'المنتجات المرتبطة',
    'product'               => 'المنتج',
    'quantity'              => 'الكمية',
    'total'                 => 'المجموع',
    'purchase_price'        => 'سعر الشراء',
    'price'                 => 'السعر',
    'sale_price'            => 'سعر البيع',
    'stock'                 => 'المخزن',
    'profit_percent'        => 'المكسب',

    'logout'                => 'تسجيل الخروج',

    'ar'                    => [
                                'name'          => 'الاسم باللغة العربية',
                                'description'   => 'الوصف باللغة العربية',
    ],

    'en'                    => [
                                'name'          => 'الاسم باللغة الانجليزية',
                                'description'   => 'الوصف باللغة الانجليزية',
    ],




];
