<?php
return [

    "dashboard"             => 'Dashboard',
    "dashboard_admin_panel" => 'Admin Panel',
    "dashboard_home"        => 'Home',
    "dashboard_contact"     => 'Contact',
    "dashboard_search"      => 'Search',
    "admin_panel"           => 'Admin Panel',
    "sales_graph"           => 'Sales Graph',
    "remember_me"           => 'Remember Me',
    "sign_in_to_start"      => 'Remember Me',
    "login"                 => 'Login',
    "add"                   => 'Add',
    "edit"                  => 'Edit',
    "delete"                => 'Delete',
    "create"                => 'Create',
    "update"                => 'Update',
    "read"                  => 'Show',

    "no_data_found"         => 'No data recorded',
    "no_records"            => 'No data recorded',

    'clients'               => 'Clients',
    'client_name'           => 'Client Name',
    'phone'                 => 'Phone',
    'address'               => 'Address',
    'previous_orders'       => 'Previous Orders',
    'orders'                => 'Orders',
    'add_order'             => 'Add Order',
    'edit_order'            => 'Edit Order',
    'order'                 => 'Order',
    'show'                  => 'Show',
    'loading'               => 'Loading ...',
    'print'                 => 'Print',

    "users"                 => 'Users',
    "first_name"            => 'First Name',
    "last_name"             => 'Last Name',
    "email"                 => 'Email',
    "options"               => 'Control',
    "users_table"           => 'Users Table',
    "search"                => 'Search',
    "password"              => 'Password',
    "password_confirmation" => 'Password Confirmation',
    "add_user"              => 'Add User',
    "image"                 => 'Image',

    "added_successfully"    => 'Your data is saved successfully',
    "edited_successfully"   => 'Your data is edited successfully',
    "deleted_successfully"  => 'Your data is deleted successfully',

    'permissions'           => 'Permissions',

    'categories'            => 'Categories',
    'all_categories'        => 'All Categories',
    'categories_table'      => 'Categories Table',
    'add_category'          => 'Add Category',
    'name'                  => 'Name',
    'description'           => 'Description',
    'products_count'        => 'Product Counts',
    'related_products'      => 'Related Products',
    'category'              => 'Category',
    'show_products'         => 'View Products',
    'created_at'            => 'Created At',

    'no'                    => 'No',
    'yes'                   => 'Yes',
    'confirm_delete'        => 'Are you sure delete that ?',

    'products'              => 'Products',
    'product'               => 'Product',
    'quantity'              => 'Quantity',
    'total'                 => 'Total',
    'purchase_price'        => 'Purchase Price',
    'price'                 => 'Price',
    'sale_price'            => 'Sale Price',
    'stock'                 => 'Stock',
    'profit_percent'        => 'Profit',
    'logout'                => 'Sign Out',

    'ar'                    => [
        'name'          => 'Arabic Name',
        'description'   => 'Arabic Description',
    ],

    'en'                    => [
        'name'          => 'English Name',
        'description'   => 'English Description',
    ],


];
